package com.example.rent.addcontrolprogramically;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private RelativeLayout relative;
    private int clickCounter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        clickCounter = 0;
        relative = (RelativeLayout) findViewById(R.id.Main);





    }





    public void definedMethod (View v) {

        final TextView dodawanaKontrolka = new TextView(relative.getContext());
        RelativeLayout.LayoutParams parametry = new RelativeLayout.LayoutParams(200, 300);
        parametry.topMargin = 200 * clickCounter;

        dodawanaKontrolka.setOnClickListener(new View.OnClickListener() {
            int licznik = 0;
            @Override
            public void onClick(View v) {
                dodawanaKontrolka.setText("Kontrolka została kliknięta" + ++licznik);
            }
        });

        dodawanaKontrolka.setLayoutParams(parametry);
        dodawanaKontrolka.setText("SDA jest spoko");
        relative.addView(dodawanaKontrolka);
        clickCounter++;




    }
}
